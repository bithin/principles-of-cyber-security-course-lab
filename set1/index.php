<?php
include('config/config.php');
echo "<html>
<head>
  <title>The Grey</title>
  <meta http-equiv=\"content-type\" content=\"text/html; charset=UTF-8\" />
  <link rel=\"stylesheet\" type=\"text/css\" href=\"css/style.css\" />
  <link rel=\"stylesheet\" type=\"text/css\" href=\"css/style.min.css\" />
  <script type=\"text/javascript\" src=\"js/modernizr-1.5.min.js\"></script>
</head>

<body>
  <div id=\"main\">";

include('header.php');

echo "<div id=\"site_content\">
        <ul class=\"slideshow\">

		    <li class=\"show\"><img width=\"950\" height=\"350\" src=\"images/1.jpg\" alt=\"&quot;Once more into the fray. into the last good fight I'll ever know.&quot;\" /></li>

		    <li><img width=\"950\" height=\"350\" src=\"images/2.jpg\" alt=\"&quot;Live and Die on this day.&quot;\" /></li>

		    <li><img width=\"950\" height=\"350\" src=\"images/3.jpg\" alt=\"&quot;Live and Die on this day.&quot;\" /></li>	
            </ul>";

if(isset($_GET['page'])){
    include('pages/'.$_GET['page'].'');
}
else {
    include('pages/home.php');
}

echo "</div>";
include('footer.php');
echo "<div id=\"popup\">
	<div class=\"content\"></div>
	</div>
</div>";
echo "<script type=\"text/javascript\" src=\"js/jquery.min.js\"></script>
<script type=\"text/javascript\" src=\"js/jquery.easing.min.js\"></script>
<script type=\"text/javascript\" src=\"js/jquery.lavalamp.min.js\"></script>
<script type=\"text/javascript\" src=\"js/jquery.bpopup-0.8.0.min.js\"></script>
<script type=\"text/javascript\" src=\"js/image_fade.js\"></script>
<script type=\"text/javascript\" src=\"js/secret.js\"></script>
<script type=\"text/javascript\">
$(function() {

    $(\"#lava_menu\").lavaLamp({

        fx: \"backout\",

            speed: 700

    });

});

</script>
</body>
</html>";

?>
